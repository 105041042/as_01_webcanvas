var canvas = document.getElementById('canvas');
var textBox = document.getElementById("textBox");
var ctx = canvas.getContext('2d');
var mode = 'brush';
ctx.strokeStyle = 'black';
var fill = false;
// for rectangle trangle circle
var startX ;
var startY ;
var currentX ;
var currentY ;

var undostack = []; 
var redostack = [];

ctx.linecap = 'round';
ctx.linejoin = 'round';

//init
var typing = false;
var textContent = "";
ctx.fillStyle = 'white';
ctx.fillRect(0,0,800,800);
ctx.fillStyle = 'black';
var img = ctx.getImageData(0,0,1000,1000);
undostack.push(img);

//color
$('.color input').change(function(){
    r = $('#red').val();
    g = $('#green').val();
    b = $('#blue').val();
    changeColor(r,g,b);
  });
  
  function changeColor(r,g,b){
    colors = {
        red : r,
        green : g,
        blue : b
      }
    $.each(colors, function(_color, _value) {
      $('#v'+_color).val(_value);
    });
    ctx.strokeStyle = "rgb("+r+","+g+","+b+")" ;
    ctx.fillStyle = "rgb("+r+","+g+","+b+")" ;
  };

mode = 'text';
var x = 0;
var y = 0;


function mouseMoveHendler(e){
    if(mode=='brush'){
        isUndo = 0;
        ctx.lineTo(e.offsetX,e.offsetY);
        ctx.stroke();
    }else if(mode=='eraser'){
        isUndo = 0;
        ctx.lineTo(e.offsetX,e.offsetY);
        ctx.stroke();
    }else if(mode=='rectangle'){
        isUndo = 0;
        currentX = e.offsetX;
        currentY = e.offsetY;
        var img = undostack[undostack.length-1];
        ctx.putImageData(img,0,0); 
        if(fill==true)
        ctx.fillRect(startX,startY,currentX-startX,currentY-startY);
        else{
            ctx.strokeRect(startX,startY,currentX-startX,currentY-startY);
            //console.log('%d %d %d %d %s',startX,startY,currentX,currentY,ctx.fillStyle);
        }
    }else if(mode=='triangle'){
        //console.log('work');
        isUndo = 0;
        var img = undostack[undostack.length-1];
        ctx.putImageData(img,0,0);
        ctx.beginPath();
        ctx.moveTo(startX,startY);
        ctx.lineTo(startX,e.y);
        ctx.lineTo(e.x,e.y);
        ctx.closePath();
        if(fill){
            ctx.fill();
        }else{
            ctx.stroke();
        }
    }else if(mode=='circle'){
        isUndo = 0;
        var img = undostack[undostack.length-1];
        ctx.putImageData(img,0,0);
        ctx.beginPath();
        ctx.arc(startX,startY,Math.sqrt((e.x-startX)*(e.x-startX)+(e.y-startY)*(e.y-startY)),0,2*Math.PI);
        ctx.closePath();
        if(fill){
            ctx.fill();
        }else{
            ctx.stroke();
        }
    }
};

canvas.addEventListener('mousedown',function(e){
    if(mode=='brush'||mode=='eraser'){
        ctx.beginPath();
        ctx.moveTo(e.offsetX,e.offsetY);
        canvas.addEventListener('mousemove',mouseMoveHendler,false);
    }else if(mode=='rectangle'||mode=='triangle'||mode=='circle'){
        startX = e.offsetX;
        startY = e.offsetY;
        canvas.addEventListener('mousemove',mouseMoveHendler,false);
    }else if(mode=='text'){
        if (typing) {
            textContent = textBox.value;
            //console.log(textContent);
            typing = false;
            textBox.value = "";
            ctx.font = "28px orbitron";
            ctx.fillText(textContent, parseInt(textBox.style.left), parseInt(textBox.style.top));
            ctx.restore();
            ctx.closePath();
        } else if (!typing) {
            typing = true;
            textBox.style.left = e.offsetX + 'px';
            textBox.style.top = e.offsetY + 'px';
        }
    }
});

canvas.addEventListener('mouseup',function(){
    var img = ctx.getImageData(0,0,1000,1000);
    undostack.push(img);
    canvas.removeEventListener('mousemove',mouseMoveHendler,false);
},false);

function changeFill(){
    if(fill==false) fill = true;
    else fill = false;
};

function brush(){
    ctx.strokeStyle = 'black';
    document.getElementById('canvas').style.cursor = 'url("image/paint-brush.png") , default';
    mode = 'brush';
};

function eraser(){
    ctx.strokeStyle = 'white';
    document.getElementById('canvas').style.cursor = 'url("image/paint-eraser.png") , default';
    mode = 'eraser';
};

function rectangle(){
    document.getElementById('canvas').style.cursor = 'crosshair';
    mode = 'rectangle';
};

function triangle(){
    document.getElementById('canvas').style.cursor = 'crosshair';
    mode = 'triangle';
};

function circle(){
    document.getElementById('canvas').style.cursor = 'crosshair';
    mode = 'circle';
};

function text(){
    document.getElementById('canvas').style.cursor = 'text';
    mode = 'text';
};


function undo(){
    if(undostack.length>1){
        redostack.push(undostack[undostack.length-1]);
        undostack.pop();
        var img2 = undostack[undostack.length-1]; 
        ctx.putImageData(img2,0,0);
        isUndo = 1;
    } 
};

function redo(){
    //console.log(isUndo);
    isUndo = 1;
    if(redostack.length>0&&isUndo){
        var img1 = ctx.getImageData(0,0,1000,1000);
        undostack.push(img1); 
        ctx.putImageData(redostack[redostack.length-1],0,0);
        redostack.pop();
    } 
};

function reset(){
    isUndo = 0;
    ctx.putImageData(undostack[0],0,0);
    undostack.splice(0,0);
    redostack.splice(0,0);
    var img = ctx.getImageData(0,0,1000,1000);
    undostack.push(img);
};

$('#save').on('click', function(){
    var _url = canvas.toDataURL();
    this.href = _url;
  });
